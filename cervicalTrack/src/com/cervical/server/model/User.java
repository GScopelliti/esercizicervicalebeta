package com.cervical.server.model;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.PrimaryKey;
import javax.persistence.Id;

import com.googlecode.objectify.annotation.Entity;

@Entity
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@PrimaryKey
	private Long id;
	private String name;
	private String email;
	private String year;
	private String gender;
	private String address;
	private Date creationDate;
	private Date lastUpdate;

	public User() {
		super();
	}

	public User(String name, String email, String year, String gender, String address) {
		super();
		this.name = name;
		this.email = email;
		this.year = year;
		this.gender = gender;
		this.address = address;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
