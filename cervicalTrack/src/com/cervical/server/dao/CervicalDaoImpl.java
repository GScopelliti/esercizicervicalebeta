package com.cervical.server.dao;

import java.util.Date;

import com.cervical.server.model.User;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

@Singleton
public class CervicalDaoImpl extends BaseDaoGaeImpl implements CervicalDao {

	static {
		ObjectifyService.register(User.class);
	}
	
	@Override
	public User save(User o) {
		if (o.getId() == null) {
			o.setCreationDate(new Date());
		}
		o.setLastUpdate(new Date());
		getService().put(o);
		return o;
	}

}
