package com.cervical.server.dao;

import com.cervical.server.model.User;

public interface CervicalDao {

	User save(User o);
	
}
