package com.cervical.server.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BaseDaoGaeImpl {

	protected Logger log = Logger.getLogger(BaseDaoGaeImpl.class.getName());

	public static Objectify getService() {
		return ObjectifyService.begin();
	}

	public <T> List<T> queryByField(Class<T> clazz, String fieldName,
			Object fieldValue) {
		ArrayList<T> list = (ArrayList<T>) getService().query(clazz)
				.filter(fieldName, fieldValue).list();
		log.info(String.format("#queryByField# %s(%s, %s) Found: %s ",
				clazz.getName(), fieldName, fieldValue, list.size()));
		return list;
	}

	public <T> List<T> queryByField(Class<T> clazz, String fieldName,
			Object fieldValue, String order) {
		ArrayList<T> list = (ArrayList<T>) getService().query(clazz)
				.filter(fieldName, fieldValue).order(order).list();
		log.info(String.format("#queryByField# %s(%s, %s, %s) Found: %s ",
				clazz.getName(), fieldName, fieldValue, order, list.size()));
		return list;
	}

	protected <T> List<T> queryByTwoFields(Class<T> clazz,
			String firstFieldName, Object firstFieldValue,
			String secondFieldName, Object secondFieldValue) {
		ArrayList<T> list = (ArrayList<T>) getService().query(clazz)
				.filter(firstFieldName, firstFieldValue)
				.filter(secondFieldName, secondFieldValue).list();
		log.info(String.format(
				"#queryByTwoFields# %s (%s, %s) (%s, %s) Found: %s ",
				clazz.getName(), firstFieldName, firstFieldValue,
				secondFieldName, secondFieldValue, list.size()));
		return list;
	}

}
