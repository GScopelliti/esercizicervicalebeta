package com.cervical.server.rest;

import java.util.Calendar;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.cervical.server.dao.CervicalDao;
import com.cervical.server.model.User;
import com.google.inject.Singleton;

@Singleton
@Path("service")
public class RegistrationServiceRest {

	@Inject
	private CervicalDao dao;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("registerUser")
	public User registerUser(@QueryParam("name") String name,
			@QueryParam("email") String email, @QueryParam("age") String age,
			@QueryParam("gender") String gender, @QueryParam("address") String address) {

		// convert age to year of birth
		final int yearOfBirth = convertAge(age);

		return dao.save(new User(name, email, String.valueOf(yearOfBirth),
				gender, address));
	}

	private int convertAge(String age) {
		Calendar cd = Calendar.getInstance();
		int currentYear = cd.get(Calendar.YEAR);
		int yearOfBirth = currentYear - Integer.valueOf(age);
		return yearOfBirth;
	}

}
