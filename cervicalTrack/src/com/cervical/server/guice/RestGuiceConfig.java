package com.cervical.server.guice;

import java.util.HashMap;
import java.util.Map;

import com.cervical.server.dao.CervicalDao;
import com.cervical.server.dao.CervicalDaoImpl;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class RestGuiceConfig extends GuiceServletContextListener {

	private final CervicalDao dao = new CervicalDaoImpl();
	
	@Override
	protected Injector getInjector() {
		return Guice.createInjector(new JerseyServletModule() {
			@Override
			protected void configureServlets() {

				// services
				bind(CervicalDao.class).toInstance(dao);
				
				// rest
				Map<String, String> params = new HashMap<String, String>();
				params.put("com.sun.jersey.config.property.packages",
						"com.cervical.server.rest");
				params.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

				serve("/rest/*").with(GuiceContainer.class, params);

			}
		});
	}
}